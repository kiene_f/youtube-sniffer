## README
-----------------
### INSTALLATION
Les 2 fichiers à utiliser sont **sniffer.sh** et **downloader.sh**  
Le fichier **uploader.sh** est un exemple de script d'upload  
##### PARAMETRAGES DES FICHIERS
1. Modifier la variable *dir* des fichiers **sniffer.sh** et **downloader.sh** par le chemin d'accès à ces fichiers (commande **pwd**).  
`dir="/home/fk/youtube/algov2/" `  
2. Ajouter les 3 lignes ci-dessous à votre script d'upload (exemple: **test.c**).  
Voir le fichier **uploader.sh** comme exemple.  
>export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"  
>dir="/home/fk/youtube/algov2/"  
>cd ${dir}  
  
##### TEST DES PARAMETRAGES
1. Lancer 1 fois **sniffer.sh**, prototype:  
    `./sniffer.sh votre_chaine_id chaine_id_sniffé` ou `./sniffer.sh chaine_id_sniffé`  
    *exemple de la chaine https://www.youtube.com/channel/UCximfRrAhYzTNTiNHv1Z-8A:*  
    `./sniffer.sh UCPr-0fbXbX1h55E2LuPbbZg UCximfRrAhYzTNTiNHv1Z-8A`  
    **Un fichier nommé chaine_id contenant les ID des vidéos de la chaine doit apparaitre.**  
2. Lancer plusieurs fois **downloader.sh**, prototype:  
    `./downloader.sh votre_chaine_id path+script_d_upload` ou `./downloader.sh chaine_id_sniffé path+script_d_upload`  
    *exemple avec ma chaine:*  
    `./downloader.sh UCPr-0fbXbX1h55E2LuPbbZg /home/fk/youtube/algov2/uploader.sh`  
    **Des vidéos doivent être uploadées sur la chaine et un fichier nommé chaine_id_done contenant les ID des vidéos uploader doit apparaitre.**  
##### AUTOMATISATION
1. Taper la commande `sudo crontab -e` sur le terminal du serveur  
2. Ajouter 2 lignes à la fin du fichier pour l'execution automatique des scripts shell (voir *crontab shell* sur google pour plus de détails), prototypes:  
`Minute Hour Day Month Command`  
Exemples:  
*Execute le fichier sniffer.sh toute les heures à 0m (1h00, 2h00 ...)*  
`0 * * * * /home/fk/youtube/algov2/sniffer.sh UCximfRrAhYzTNTiNHv1Z-8A >> /dev/null 2>&1`  
*Execute le fichier downloader.sh toute les heures à 5m (1h05, 2h05 ...)*  
`5 */2 * * * /home/fk/youtube/algov2/downloader.sh UCximfRrAhYzTNTiNHv1Z-8A   /home/fk/youtube/algov2/uploader.sh >> /dev/null 2>&1`