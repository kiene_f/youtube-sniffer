#!/bin/bash

export LANG=C.UTF-8
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"

dir="/home/fk/youtube/algov2/"
cd ${dir}

url="https://www.youtube.com/watch?v=$1"

youtube-dl -o '%(id)s/%(id)s.%(ext)s' ${url}
id=`youtube-dl -s --no-warnings --get-id ${url}`
cd "./${id}/"
file=`echo *`
cd ..
mkdir "./${id}-img/"
cd "./${id}-img/"
youtube-dl --write-thumbnail --skip-download ${url}
img=`echo *`
cd ..
title=`youtube-dl -s --no-warnings --get-title ${url}`
echo ${file}
echo ${img}
echo ${title}
youtube-upload --title="TPMP BestOf - ${title}" --description="Abonne-toi et like pour soutenir la nouvelle chaine des bestof TPMP!" --tags="tv, tpmp, tpmp bestof, touche pas à mon poste, Benoit, Capucine, Cyril hanouna, lemoine, christophe lemoine, Capucine Anav, Enora, Mathieu delormeau, Matthieu delormeau, ${title// /,}" --default-language="fr" --default-audio-language="fr" --client-secrets=my_client_secrets.json --credentials-file=my_credentials.json --thumbnail "./${id}-img/${img}" "./${id}/${file}"
rm -r "./${id}/"
rm -r "./${id}-img/"
