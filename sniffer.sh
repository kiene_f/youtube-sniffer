#!/bin/bash

export LANG=C.UTF-8
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"

dir="/home/fk/youtube/algov2/"
cd ${dir}

if (( $# < 2 ));
then
	id=$1
	id_sniffer=$1
else
	id=$1 # chaine_id de votre chaine
	id_sniffer=$2 # chaine_id à de l chaine sniffer
fi

youtube-dl --get-id -i -q --no-warnings --min-views 1000 --playlist-start 1 --playlist-end 25 --playlist-reverse https://www.youtube.com/channel/${id_sniffer}/videos > ${id}_tmp # télécharge la liste des ID des vidéos de la chaine dans le fichier id_tmp

if [ -s "${id}_done" ] && [ -s "${id}" ]; # si des vidéos on déjà été uploadé
then
	awk 'FNR==NR{a[$0]++;next}(!($0 in a))' ${id}_done ${id}_tmp > ${id}_tmp2 # compare et supprime les vidéo déjà uploadé sur notre chaine de la liste
	awk 'FNR==NR{a[$0]++;next}(!($0 in a))' ${id} ${id}_tmp2 >> ${id} # compare et supprime les vidéo déjà présente dans le fichier id afin d'ajouter les nouvelles vidéos de la chaine sniffé
	rm ${id}_tmp
	rm ${id}_tmp2
elif [ -s "${id}" ]; 
then
	awk 'FNR==NR{a[$0]++;next}(!($0 in a))' ${id} ${id}_tmp >> ${id} # compare et supprime les vidéo déjà présente dans le fichier id afin d'ajouter les nouvelles vidéos de la chaine sniffé
	rm ${id}_tmp
elif [ -s "${id}_done" ]; 
then
	awk 'FNR==NR{a[$0]++;next}(!($0 in a))' ${id}_done ${id}_tmp > ${id}
	rm ${id}_tmp
else
	cat ${id}_tmp >> ${id}
	rm ${id}_tmp
fi
