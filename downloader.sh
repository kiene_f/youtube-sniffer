#!/bin/bash

export LANG=C.UTF-8
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"

dir="/home/fk/youtube/algov2/"
cd ${dir}

id=$1 # get chaine_id


line=$(head -1 ${id}) # print la 1er ligne du fichier nommé chaine_id

if [ -n "$line" ]; then # Si la 1er ligne existe, si le fichier n'est pas vide
	echo ${line} >> ${id}_done # Ajoute la 1er ligne à la fin du fichier chaine_id_done
	echo "$(tail -n +2 ${id})" > ${id} # supprime la 1er ligne du fichier chaine_id
    $2 ${line} # Lance le script d'upload (test.c) avec comme arguement la 1er ligne (l'id de la vidéo)
else
    echo "$(tail -n +2 ${id})" > ${id}
    exit
fi


